//
// Created by dragoon on 28.11.2020.
//

#include "MyEntity.h"
#include "Data.h"

std::vector<EntityProperties> MyEntity::props;

MyEntity::MyEntity(const MyEntity &entity) {
    *this = entity;
}

MyEntity::MyEntity(const Entity &entity) {
    this->id = entity.id;
    this->entityType = entity.entityType;
    this->playerId = entity.playerId ? *entity.playerId : 0;
    this->health = entity.health;
    this->position = entity.position;
    this->active = entity.active;
    this->actionsCount = 0;
    this->state = 0;
    this->action = nullptr;
    this->nextPt = {-1, -1};
}

MyEntity::MyEntity(const Entity &entity, int seenTick): MyEntity(entity) {
    this->lastSeen = seenTick;
}


MyEntity::MyEntity(const MyEntity &entity, const Vec2Int &pos) {
    this->id = -1;
    this->entityType = entity.entityType;
    this->playerId = entity.playerId;
    this->health = MyEntity::props[entityType].maxHealth;
    this->position = pos;
    this->active = true;
    this->actionsCount = 0;
    this->action = nullptr;
    this->state = 0;
    this->lastSeen = 0;
    this->nextPt = {-1, -1};
}

int MyEntity::size() const {
    return props[this->entityType].size;
}

Vec2Double MyEntity::center() const {
    const auto &size = this->size();
    return {(position.x + position.x + size) / 2.,
            (position.y + position.y + size) / 2.};
}

int MyEntity::attackRange() const {
    return props[this->entityType].attack->attackRange;
}

int MyEntity::attackDamage() const {
    return props[this->entityType].attack->damage;
}

int MyEntity::distanceToEntity(const Vec2Int &entityPos) const {
    return distanceToEntity(Data::getUnsafe(entityPos));
}

// works from units to buildings
int MyEntity::distanceToEntity(const MyEntity *other) const {
    int size = other->size();
    int dX = this->position.x - other->position.x;
    if (dX > 0) {
        dX = std::max(0, dX - size + 1);
    }
    int dY = this->position.y - other->position.y;
    if (dY > 0) {
        dY = std::max(0, dY - size + 1);
    }
    return std::abs(dX) + std::abs(dY);
}

int MyEntity::canMove() const {
    return props[this->entityType].canMove;
}

int MyEntity::maxHealth() const {
    return props[this->entityType].maxHealth;
}

MyEntity &MyEntity::operator=(const MyEntity &entity) {
    this->id = entity.id;
    this->entityType = entity.entityType;
    this->playerId = entity.playerId;
    this->health = entity.health;
    this->position = entity.position;
    this->active = entity.active;
    this->actionsCount = 0;
    this->action = nullptr;
    this->state = 0;
    this->lastSeen = entity.lastSeen;
    this->nextPt = {-1, -1};
    return *this;
}
