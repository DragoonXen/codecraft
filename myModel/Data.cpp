//
// Created by dragoon on 28.11.2020.
//

#include "Data.h"

MyEntity *Data::map[80][80];
int Data::myId;
const PlayerView *Data::playerView;
int Data::healCounter;
int Data::mapResourceExpected;
int Data::maxEntityId;
int Data::mapSize;
int Data::maxRetries;
int Data::tickEnemyBarracksStarted;
int Data::expectedResource;
bool Data::emergencyMode;

std::vector<int> Data::topSaboteurs;
std::vector<int> Data::rightSaboteurs;

int Data::lastStepId;
std::unordered_map<int, int> Data::unitStepId;
std::unordered_map<int, Vec2Int> Data::lastResourceAim;

int Data::kills[TURRET + 1];
int Data::attack[TURRET + 1];
int Data::overDamage;
int Data::damageReceived[TURRET + 1];
int Data::deaths[TURRET + 1];
PlayerData Data::me;
std::vector<PlayerData> Data::others;
std::vector<MyEntity> Data::resources;