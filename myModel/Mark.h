//
// Created by dragoon on 30.11.2020.
//

#ifndef AICUP2020_MARK_H
#define AICUP2020_MARK_H


#include "Data.h"

enum FieldType {
    FOOD_DISTANCE = 0, // std::numeric_limits<int>::max()
    ENEMY_INFLUENCE_FIELD = FOOD_DISTANCE + 1, // std::numeric_limits<int>::max()
    MY_WORKERS_DISTANCE_FIELD = ENEMY_INFLUENCE_FIELD + 1, // std::numeric_limits<int>::max()
    MY_PEACEFUL_OBJECTS_DISTANCE_FIELD = MY_WORKERS_DISTANCE_FIELD + 1, // std::numeric_limits<int>::max()
    ENEMY_WORKERS_DISTANCE_FIELD = MY_PEACEFUL_OBJECTS_DISTANCE_FIELD + 1, // std::numeric_limits<int>::max()
    MY_DISTANCE_FIELD = ENEMY_WORKERS_DISTANCE_FIELD + 1, // Mark::field[i].fill(Mark::NO_ENEMY_DISTANCE);
    ENEMY_MIL_DISTANCE_FIELD = MY_DISTANCE_FIELD + 1, // Mark::field[i].fill(Mark::NO_ENEMY_DISTANCE);
    ENEMY_MIL_NO_TURRETS_DISTANCE = ENEMY_MIL_DISTANCE_FIELD + 1, // Mark::field[i].fill(Mark::NO_ENEMY_DISTANCE);
    ENEMY_CIVILIAN_DISTANCE_FIELD = ENEMY_MIL_NO_TURRETS_DISTANCE + 1, // Mark::field[i].fill(Mark::NO_ENEMY_DISTANCE);
    ENEMY_DANGER_FIELD = ENEMY_CIVILIAN_DISTANCE_FIELD + 1, // Mark::field[i].fill(0);
    ENEMY_SWORDSMAN_DANGER_FIELD = ENEMY_DANGER_FIELD + 1, // Mark::field[i].fill(0);
    ENEMY_DANGER_EXTENDED_FIELD = ENEMY_SWORDSMAN_DANGER_FIELD + 1, // Mark::field[i].fill(0);
    ENEMY_DANGER_EXTENDED_FIELD_2 = ENEMY_DANGER_EXTENDED_FIELD + 1, // Mark::field[i].fill(0);
    PATH_HOTSPOT = ENEMY_DANGER_EXTENDED_FIELD_2 + 1,
    LAST_PATH_HOTSPOT = PATH_HOTSPOT + 1,
    BLOCK_STATIC = LAST_PATH_HOTSPOT + 1,
    RESOURCE_DIG_MARK = BLOCK_STATIC + 1,
    INFLUENCE_FIELD = RESOURCE_DIG_MARK + 1,
    VISIBLE = INFLUENCE_FIELD + 1,
    LAST_SAW = VISIBLE + 1,
    CIV_TIME_TO_GO = LAST_SAW + 1,
    MIL_TIME_TO_GO = CIV_TIME_TO_GO + 1,
    BREAKTHROUGH_PRIORITY_FIELD = MIL_TIME_TO_GO + 1,
    REPAIR_PLACES = BREAKTHROUGH_PRIORITY_FIELD + 1

//    ME_DANGER_FIELD = 14,
//    ME_DANGER_EXTENDED_FIELD = 15,
//    ME_DANGER_EXTENDED_FIELD_2 = 16

};

class Mark {
public:
    static const int NO_ENEMY_DISTANCE = 10000;

    int mark[80][80];
    int currentMark;

    static Mark instances[12];
    static Mark field[REPAIR_PLACES + 1];

    Mark();

    bool putMark(const Vec2Int &pos);

    void fill(int value);

    void copy(Mark &other);

    bool checkMark(const Vec2Int &pos) const;

    bool checkMarkUnsafe(const Vec2Int &pos) const;

    int getMarkUnsafe(const Vec2Int &pos) const;

    void putValUnsafe(const Vec2Int &pos, int value);

    void justPutMark(const Vec2Int &pos);

    void justPutMarkUnsafe(const Vec2Int &pos);

    bool newMinimumUnsafe(const Vec2Int &pos, int value);

    bool putMarkUnsafe(const Vec2Int &anInt);

    void addUnsafe(const Vec2Int &pos, int val);

    void incrementUnsafe(const Vec2Int &anInt);

    void decrementUnsafe(const Vec2Int &anInt);

    bool nonZeroUnsafe(const Vec2Int &pos);
};


#endif //AICUP2020_MARK_H
