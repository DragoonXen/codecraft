//
// Created by dragoon on 28.11.2020.
//

#ifndef AICUP2020_MYENTITY_H
#define AICUP2020_MYENTITY_H


#include "../model/Entity.hpp"
#include "../model/EntityProperties.hpp"
#include "MyAction.h"
#include "../model/Vec2Double.h"

class MyEntity {

public:
    static std::vector<EntityProperties> props;
    int playerId;
    EntityType entityType;
    int lastSeen;
    int health;
    int actionsCount;
    Vec2Int position;
    bool active;
    int state;
    int id;
    Vec2Int nextPt;
    std::unique_ptr<MyAction> action;

    MyEntity(const MyEntity &entity);

    explicit MyEntity(const Entity &entity);

    MyEntity(const Entity &entity, int seenTick);

    MyEntity(const MyEntity &entity, const Vec2Int &pos);

    Vec2Double center() const;

    int size() const;
    int attackRange() const;
    int attackDamage() const;
    int canMove() const;

    inline bool moved() const {
        return nextPt.x >= 0;
    }

    int distanceToEntity(const Vec2Int &entityPos) const;
    int distanceToEntity(const MyEntity* entity) const;

    int maxHealth() const;

    MyEntity& operator=(const MyEntity& entity);
};


#endif //AICUP2020_MYENTITY_H
