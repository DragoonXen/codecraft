//
// Created by dragoon on 26.12.2020.
//

#ifndef AICUP2020_BARRACKSBUILDING_H
#define AICUP2020_BARRACKSBUILDING_H


namespace BarracksBuilding {

    const int MAX_WORKERS_SEARCH_DISTANCE = 16;
    int buildTable[10][MAX_WORKERS_SEARCH_DISTANCE] = {{1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1}, //WALL
                                                       {6,  6,  5,  4,  3,  3,  3,  2,  2,  2,  2,  1,  1,  1,  1,  1}, //HOUSE
                                                       {10, 10, 10, 10, 10, 10, 9,  8,  7,  6,  5,  5,  5,  5,  5,  5}, //BUILDER_BASE
                                                       {1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1}, //BUILDER_UNIT
                                                       {10, 10, 10, 10, 10, 10, 9,  8,  7,  6,  5,  5,  5,  5,  5,  5}, //MELEE_BASE
                                                       {0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0}, //MELEE_UNIT
                                                       {15, 15, 15, 15, 14, 14, 14, 14, 13, 13, 13, 13, 12, 12, 12, 12}, //RANGED_BASE
                                                       {0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0}, //RANGED_UNIT
                                                       {0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0}, //RESOURCE
                                                       {8,  8,  6,  4,  4,  3,  3,  2,  2,  2,  2,  0,  0,  0,  0,  0}}; //TURRET

    void repairBuilding(PlayerData &data, const MyEntity &building) {
        auto &repairPoints = Mark::instances[1];
        auto &marker = Mark::instances[2];
        auto &staticBlockField = Mark::field[BLOCK_STATIC];

        bool enemy = building.playerId != Data::myId;

        int toRepair = enemy ? building.health
                             : (MyEntity::props[building.entityType].maxHealth - building.health);
        if (toRepair == 0) {
            return;
        }

        auto freeNeighbours = Utils::neighbours(building.position, building.entityType);
        Utils::eraseMatched<Vec2Int>(freeNeighbours, [&data](const Vec2Int &pos) {
            if (Mark::field[REPAIR_PLACES].checkMark(pos) ||
                Mark::field[ENEMY_DANGER_EXTENDED_FIELD].getMarkUnsafe(pos) > 0) {
                // не подходим на расстояние вытянутого меча
                return true;
            }
            auto entity = Data::get(pos);
            return entity != nullptr &&
                   (entity->entityType != BUILDER_UNIT || entity->playerId != data.id());
        });
        ++repairPoints.currentMark;
        for (const auto &pt : freeNeighbours) {
            repairPoints.putMarkUnsafe(pt);
            staticBlockField.putValUnsafe(pt, staticBlockField.currentMark - 1);
//                    MapDrawing::drawSquare(pt, 0xFF0000, 5);
        }
        ++marker.currentMark;
        Utils::doForAllInUnitRange(1, building.position, building.size(),
                                   [&marker](const Vec2Int &pt) {
                                       marker.putMarkUnsafe(pt);
                                   });

        int totalWorkers = 0;
        int step = 0;
        auto expanding = freeNeighbours;

        while (true) {
            for (const auto &pt : expanding) {
//                        MapDrawing::drawSquare(pt, 0xFF0000 | ((25 * step + 25) << 24), 9);
                auto entity = Data::getUnsafe(pt);
                totalWorkers += (entity != nullptr && entity->entityType == BUILDER_UNIT &&
                                 entity->playerId == data.id());
            }
            if (buildTable[building.entityType][step] <= totalWorkers) {
                break;
            }
            if (++step >= MAX_WORKERS_SEARCH_DISTANCE) {
                break;
            }
            if ((totalWorkers + 1) * step >= toRepair) {
                --step;
                break;
            }
            Utils::expandBordersIgnoreMoveable(expanding, marker);
        }
        if (step >= MAX_WORKERS_SEARCH_DISTANCE) {
            step = MAX_WORKERS_SEARCH_DISTANCE - 1;
        }

        Utils::forceMoveToPoints(repairPoints,
                                 data.workers,
                                 enemy ?
                                 [](const MyEntity &entity) {
                                     return entity.action && (entity.action->actionType == REPAIR) || entity.health < 4;
                                     // || entity.action->actionType == BUILD
                                 } :
                                 [](const MyEntity &entity) {
                                     return entity.action && (entity.action->actionType == REPAIR);
                                     // || entity.action->actionType == BUILD
                                 },
                                 [buildingType = building.entityType](const Vec2Int &pos, MyEntity &entity) {
                                     if (entity.action && entity.action->actionType == BUILD) {
                                         return;
                                     }
                                     if (entity.action &&
                                         entity.action->actionType == DIG &&
                                         entity.action->target == entity.position) {
                                         --Data::me.player.resource;
                                     }
                                     Mark::field[REPAIR_PLACES].putMarkUnsafe(pos);
                                     entity.action = std::make_unique<MyAction>(ActionType::REPAIR,
                                                                                pos,
                                                                                buildingType);
                                 }, std::min(buildTable[building.entityType][step], toRepair), step + 10);
    }

    inline int apply_scores(int baseScore, const Vec2Int &pos, int playerId) {
        auto entity = Data::get(pos);
        if (entity == nullptr) {
            return 0;
        }
        if (entity->playerId && entity->playerId != playerId) {
            return -200 * baseScore;
        }
        if (entity->entityType != BUILDER_UNIT) {
            return 0;
        }
        if (!entity->action) {
            return 2 * baseScore;
        } else {
            if (entity->action->actionType == REPAIR) { // уже занят
                return 0;
            }
            return baseScore;
        }
    }

    static int checkBarracksPassage(Mark &refer, Mark &forUse, const Vec2Int &pos, int size) {
        Utils::putFakeObstacle(pos, size);
        Utils::distanceFromBase(forUse);
        Utils::clearFakeObstacle(pos, size);

        int cnt = 0;
        for (const auto &pt : Utils::neighbours(pos, size)) {
            if (!Data::insideMap(pt)) {
                continue;
            }
            if (refer.getMarkUnsafe(pt) == std::numeric_limits<int>::max()) {
                continue;
            }
            auto diff = forUse.getMarkUnsafe(pt) - refer.getMarkUnsafe(pt);
            if (diff < 8) {
                ++cnt;
            }
        }
        return cnt;
    }

    inline bool isBuilding(const MyEntity *entityToCheck) {
        return entityToCheck != nullptr && entityToCheck->entityType != RESOURCE && !entityToCheck->canMove();
    }

    inline bool checkIfBlocked(const Vec2Int &pos, Mark &marker) {
        auto entityToCheck = Data::getUnsafe(pos);
        if (isBuilding(entityToCheck)) {
            return false; // это и так здание, тут ловить нечего
        }
        int cnt = 1;
        std::queue<Vec2Int> queue;
        queue.push(pos);
        while (!queue.empty() && cnt < 20) {
            auto curr = queue.front();
            queue.pop();
            for (const auto &ngb : curr.neighbours()) {
                if (marker.putMark(ngb)) { // чёт новенькое...
                    entityToCheck = Data::getUnsafe(pos);
                    if (isBuilding(entityToCheck)) {
                        continue;
                    }
                    queue.push(ngb);
                    ++cnt;
                }
            }
        }
        return cnt < 20;
    }

    inline static bool checkBuildingsNearby(const Vec2Int &pos, int houseSize) {
        for (int i = -1; i != houseSize; ++i) {
            auto entity = Data::get(pos.x + i, pos.y - 1); // x - -1 0 1 2
            if (entity != nullptr && entity->entityType != UNKNOWN && entity->entityType != RESOURCE &&
                !entity->canMove()) {
                if ((entity->entityType != BUILDER_BASE || entity->position != Vec2Int(5, 5)) &&
                    (!pos.oneIsZero() || !entity->position.oneIsZero())) {
                    return true;
                }
            }
            entity = Data::get(pos.x + houseSize, pos.y + i); // y - -1 0 1 2
            if (entity != nullptr && entity->entityType != UNKNOWN && entity->entityType != RESOURCE &&
                !entity->canMove()) {
                if ((entity->entityType != BUILDER_BASE || entity->position != Vec2Int(5, 5)) &&
                    (!pos.oneIsZero() || !entity->position.oneIsZero())) {
                    return true;
                }
            }
            entity = Data::get(pos.x - 1, pos.y + i + 1); // y - 0 1 2 3
            if (entity != nullptr && entity->entityType != UNKNOWN && entity->entityType != RESOURCE &&
                !entity->canMove()) {
                if ((entity->entityType != BUILDER_BASE || entity->position != Vec2Int(5, 5)) &&
                    (!pos.oneIsZero() || !entity->position.oneIsZero())) {
                    return true;
                }
            }
            entity = Data::get(pos.x + i + 1, pos.y + houseSize); // x - 0 1 2 3
            if (entity != nullptr && entity->entityType != UNKNOWN && entity->entityType != RESOURCE &&
                !entity->canMove()) {
                if ((entity->entityType != BUILDER_BASE || entity->position != Vec2Int(5, 5)) &&
                    (!pos.oneIsZero() || !entity->position.oneIsZero())) {
                    return true;
                }
            }
        }
        if (pos.x == 0 && pos.y == 0) {
            return false;
        }
        if (pos.x == 0 && pos.y <= houseSize) {
            for (int i = 0; i != pos.y; ++i) {
                auto entity = Data::getUnsafe(houseSize, i);
                if (entity == nullptr || entity->entityType == RESOURCE || entity->canMove()) {
                    return false;
                }
            }
            return true;
        }
        if (pos.x <= houseSize && pos.y == 0) {
            for (int i = 0; i != pos.x; ++i) {
                auto entity = Data::getUnsafe(i, houseSize);
                if (entity == nullptr || entity->entityType == RESOURCE || entity->canMove()) {
                    return false;
                }
            }
            return true;
        }
        return false;
    }

    inline bool checkIfBlockedSmthByBuildings(const Vec2Int &position, int size) {
        auto &marker = Mark::instances[0];
        Utils::putFakeObstacle(position, size);
        for (const auto &pt: Utils::neighbours(position, size)) {
            ++marker.currentMark;
            if (marker.putMark(pt)) {
                if (checkIfBlocked(pt, marker)) {
                    Utils::clearFakeObstacle(position, size); // дублирование, но фиг с ним
                    return true;
                }
            }
        }
        Utils::clearFakeObstacle(position, size); // дублирование, но фиг с ним
        return false;
    }

    inline bool checkBuildBarrackPlace(const Vec2Int &pos,
                                       int playerId,
                                       EntityType entityType,
                                       std::vector<std::pair<long long, Vec2Int>> &places) {
        int milDistance = Mark::field[ENEMY_MIL_NO_TURRETS_DISTANCE].mark[pos.x + 1][pos.y + 1];
        if (milDistance < 50) {
            return false;
        }
        if (Data::others.size() == 1) {
            std::vector<Vec2Int> barrackCheckpoints;
            barrackCheckpoints.emplace_back(pos.x, pos.y);
            barrackCheckpoints.emplace_back(pos.x + 2, pos.y);
            barrackCheckpoints.emplace_back(pos.x, pos.y + 2);
            barrackCheckpoints.emplace_back(pos.x + 4, pos.y + 4);
            barrackCheckpoints.emplace_back(pos.x + 4, pos.y + 2);
            barrackCheckpoints.emplace_back(pos.x + 2, pos.y + 4);
            auto &enemyTurrets = Data::others[0].turrets;
            int minDistance = 1000;
            for (const auto &turret : enemyTurrets) {
                Utils::doForAllPts(turret.position,
                                   turret.size(),
                                   [&minDistance, &pos, &barrackCheckpoints](const Vec2Int &ps) {
                                       for (const auto &bc : barrackCheckpoints) {
                                           minDistance = std::min(minDistance, bc.distance(ps));
                                       }
                                   });
            }
            if (minDistance < 7) {
                return false;
            }
        }
        int buildingSize = MyEntity::props[entityType].size;
        int dist = Mark::field[MY_WORKERS_DISTANCE_FIELD].getMarkUnsafe(
                pos + Vec2Int(buildingSize / 2, buildingSize / 2));
        if (dist > 17) {
            return false;
        }

        for (int x = 0; x != buildingSize; ++x) {
            for (int y = 0; y != buildingSize; ++y) {
                Vec2Int checkPos = pos + Vec2Int(x, y);
                if (Mark::field[ENEMY_DANGER_EXTENDED_FIELD_2].getMarkUnsafe(checkPos) != 0 ||
                    Data::getUnsafe(checkPos) != nullptr) {
                    return false;
                }
            }
        }
        if (Data::emergencyMode) {
            if (checkIfBlockedSmthByBuildings(pos, buildingSize)) {
                return false;
            }
        } else {
            if (checkBuildingsNearby(pos, buildingSize)) {
                return false;
            }
        }
        // скоринг переделать
        long long currScore = 0;
        auto neighbours = Utils::neighbours(pos, buildingSize);
        auto &marker = Mark::instances[0];
        Utils::fillMark(pos, buildingSize, marker);
        for (const auto &position : neighbours) {
            marker.putMark(position);
        }
        int originMaxCnt = checkBarracksPassage(Mark::instances[5], Mark::instances[6], pos, 5);
        int maxCnt = originMaxCnt;
        originMaxCnt = std::min(originMaxCnt, 15);
        int scores[MAX_WORKERS_SEARCH_DISTANCE] = {80, 75, 70, 65, 60, 55, 50, 45, 40, 35, 30, 25, 20, 15, 10, 5};
        int cnt = 0;
        for (int i = 0; i != MAX_WORKERS_SEARCH_DISTANCE; ++i) {
            for (const auto &position : neighbours) {
                int score = apply_scores(scores[i], position, playerId);
                if (score > 0) {
                    ++cnt;
                    if (cnt == maxCnt) {
                        break;
                    }
                }
                currScore += score;
            }
            maxCnt = std::min(maxCnt, buildTable[RANGED_BASE][i]);
            if (cnt >= maxCnt) {
                break;
            }
            if (i + 1 < MAX_WORKERS_SEARCH_DISTANCE) {
                Utils::expandBordersIgnoreMoveable(neighbours, marker);
            }
        }
        if (currScore <= 0) {
            return false;
        }

        long long min = 80;
        Utils::doForAllPts({33, 33}, 2, [&min, &pos, &buildingSize](const Vec2Int &pt) {
            min = std::min(min, (long long) Utils::distanceTo(pt, pos, buildingSize));
        });
        min = 67 - min; // максимальное расстояние - 66

        int totalWorkersCount = std::min(std::min(cnt, maxCnt), 13);
        int ticksToWalkRemain = std::max(
                Data::tickEnemyBarracksStarted + Utils::TICKS_ALLOW_TO_WALK - Data::playerView->currentTick, 15);
        if (!Data::emergencyMode) {
//#ifdef DEBUG_ENABLED
//            if (Data::playerView->currentTick == 214) {
//                std::cout << pos.x << ',' << pos.y << " CS " << currScore << " TW " << totalWorkersCount << " dist "
//                          << min
//                          << " Total "
//                          << currScore + ticksToWalkRemain * totalWorkersCount * min << std::endl;
//            }
//#endif
            currScore += ticksToWalkRemain * totalWorkersCount * min;
        } else {
            currScore *= min * totalWorkersCount;
        }
        if (currScore > 0) {
            places.emplace_back(currScore, pos);
            return true;
        }
        return false;
    }

    MyEntity *searchWorkerForBuilding(const Vec2Int &pos, int size) {
        auto neighbours = Utils::neighbours(pos, size);
        auto &marker = Mark::instances[0];
        Utils::fillMark(pos, size, marker);
        for (const auto &position : neighbours) {
            marker.putMark(position);
        }
        MyEntity *best = nullptr;
        int bestDist = 0;
        for (int i = 0; i != MAX_WORKERS_SEARCH_DISTANCE; ++i) {
            for (const auto &position : neighbours) {
                marker.putMark(position);
                auto entity = Data::get(position);
                if (entity == nullptr || entity->entityType != BUILDER_UNIT) {
                    continue;
                }
                if (!entity->action) {
                    return entity;
                }
                if (entity->action->actionType == BUILD || entity->action->actionType == REPAIR) {
                    continue;
                }
                if (best == nullptr) {
                    best = entity;
                    bestDist = entity->action->target.distance(entity->position);
                } else {
                    int newDist = entity->action->target.distance(entity->position);
                    if (newDist > bestDist) {
                        best = entity;
                        bestDist = newDist;
                    }
                }
            }
            if (best != nullptr) {
                return best;
            }
            if (i + 1 < MAX_WORKERS_SEARCH_DISTANCE) {
                Utils::expandBordersIgnoreMoveable(neighbours, marker);
            }
        }
        return nullptr;
    }

    static bool blockPassage(Mark &refer, Mark &forUse, const Vec2Int &pos, int size) {
        for (int i = pos.x; i != pos.x + size; ++i) {
            for (int j = pos.y; j != pos.y + size; ++j) {
#ifdef DEBUG_ENABLED
                if (Data::get({i, j}) != nullptr) {
                    std::cerr << "Тут ничего не должно быть!" << std::endl;
                    Utils::thr();
                }
#endif
                Data::map[i][j] = Data::fakeObstacle();
            }
        }
        Utils::distanceFromBase(forUse);
        for (int i = pos.x; i != pos.x + size; ++i) {
            for (int j = pos.y; j != pos.y + size; ++j) {
                Data::map[i][j] = nullptr;
            }
        }
        int blockedAllowed = size * size + 2;
        int sum1 = 0;
        int sum2 = 0;
        for (int i = 0; i != Data::mapSize; ++i) {
            for (int j = 0; j != Data::mapSize; ++j) {
                if (refer.mark[i][j] == std::numeric_limits<int>::max()) {
                    continue;
                }
                auto diff = forUse.mark[i][j] - refer.mark[i][j];
                if (diff > 1000) {
                    --blockedAllowed;
                    if (blockedAllowed < 0) {
#ifdef DEBUG_ENABLED
                        std::cout << "Blocked: (" << pos.x << ',' << pos.y << ") of size " << size
                                  << " - too many fully blocked tiles" << std::endl;
#endif
                        return true;
                    }
                } else {
                    sum1 += refer.mark[i][j];
                    sum2 += forUse.mark[i][j];
                }
            }
        }
        // увеличение на 10 % и более

        if (sum2 / (double) sum1 >= 1.20 && sum2 >= 100000) {
#ifdef DEBUG_ENABLED
            std::cout << "Blocked: (" << pos.x << ',' << pos.y << ") of size " << size
                      << " - distance growth too bad for check: sum1 " << sum1 << " sum2 " << sum2 << " over "
                      << sum2 / (double) sum1 << std::endl;

#endif
            return true;
        }
        return false;
    }

    static void build_barrack(PlayerData &data, int needed, EntityType entityType) {
        const auto &entityProps = MyEntity::props[entityType];
        std::vector<std::pair<long long, Vec2Int> > places;
        int maxPos = Data::mapSize - entityProps.size;
        auto &baseDistance = Mark::instances[5];
        if (entityType == RANGED_BASE) {
            Utils::distanceFromBase(baseDistance);
        }
        for (int i = 0; i != maxPos; ++i) {
            for (int j = 0; j != maxPos; ++j) {
                checkBuildBarrackPlace({i, j}, data.id(), entityType, places);
            }
        }
        if (places.empty()) {
            return;
        }
//    for (auto &place : places) {
//        int dist = std::min(place.second.distance({0, 0}), 25);
//        place.first *= 1. + (25 - dist) / 5.;
//    }
        const auto placesComparator = [](const auto &first, const auto &second) {
            return first.first > second.first;
        };
        std::sort(places.begin(), places.end(), placesComparator);
        int i = -1;
        if (entityType != RANGED_BASE) {
            Utils::distanceFromBase(baseDistance);
        }
        auto &newBaseDistance = Mark::instances[6];
        while (needed && data.player.resource >= entityProps.cost && ++i < (int) places.size()) {
            if (i > 0) {
                if (!checkBuildBarrackPlace(places[i].second, data.id(), entityType, places)) {
                    continue;
                }
                places[i] = *places.rbegin();
                places.pop_back();
                // ищем новое место под постройку
                if (i + 1 < (int) places.size() && places[i + 1].first > places[i].first) {
                    std::sort(places.begin() + i, places.end(), placesComparator);
                    --i;
                    continue;
                }
            }
            // тут проверить, что не закрываем проход
            if (entityType != RANGED_BASE &&
                blockPassage(baseDistance, newBaseDistance, places[i].second, entityProps.size)) {
                continue;
            }

            // а тут... уберём всю эту чушь и поставим просто первого попавшегося, видимо
            auto best = searchWorkerForBuilding(places[i].second, entityProps.size); // REPLACE WITH NEW FUNCTION!!!!!
            if (best == nullptr) {
                continue;
            }
            if (best->action && best->action->actionType == DIG && best->action->target == best->position) {
                if (data.player.resource - 1 < entityProps.cost) {
#ifdef DEBUG_ENABLED
                    std::cout << "FIRED!" << std::endl;
#endif
                    return; // хватит, думаю
                } else {
                    --data.player.resource;
                }
            }
            --needed;
            data.player.resource -= entityProps.cost;
            best->action = std::make_unique<MyAction>(ActionType::BUILD, places[i].second, entityType);
            //Entity(int id, std::shared_ptr<int> playerId, EntityType entityType, Vec2Int position, int health, bool active);
            data.newBuildings.emplace_back(Entity(-354,
                                                  std::make_shared<int>(data.id()),
                                                  entityType,
                                                  places[i].second,
                                                  0,
                                                  false));

            Data::addAllToMap(data.newBuildings);// update all each time - reallocation possible

            Utils::doForAllPts(places[i].second, entityProps.size, [](const Vec2Int &pt) {
                Mark::field[BLOCK_STATIC].putMarkUnsafe(pt);
            });
            if (data.player.resource > 0) {
                // сразу отправляем рабочих, если ресурсов было не впритык
                repairBuilding(data, *data.newBuildings.rbegin());
            }
        }
    }
}

#endif //AICUP2020_BARRACKSBUILDING_H
