//
// Created by dragoon on 29.11.2020.
//

#ifndef AICUP2020_VEC2DOUBLE_H
#define AICUP2020_VEC2DOUBLE_H


class Vec2Double {
public:
    double x;
    double y;
    Vec2Double(double x, double y);
};


#endif //AICUP2020_VEC2DOUBLE_H
